// ==UserScript==
// @name         anonfm-html5-player
// @namespace    http://tampermonkey.net/
// @version      1.0.6
// @description  Anon.fm html5 player for TWP contest
// @author       Vnuchaev
// @license      GPL-3.0-or-later
// @include      /^https?://anon.fm/menu.html
// @grant        GM_addStyle
// @run-at       document-end
// ==/UserScript==

let player = document.querySelector('iframe[src$="play.html"]');

const newplayer = document.createElement('div');
newplayer.style.cssText = 'margin-top: 10px';

const audioSource = 'https://anon.fm/streams/radio';

let audio = new Audio();
audio.removeAttribute('preload');
audio.volume = customVolume(0.3);
function customVolume(volume){
    return Math.pow(volume, 1.7);
}

const play = document.createElement('div');
play.textContent = 'play';
play.style.cssText = 'display: inline-block; background: #ccc; height: 48px; line-height: 48px; width: 60px; cursor: pointer; margin-right: 5px';
play.addEventListener('click', function(event) {
    if(audio.paused) {
        this.textContent = 'stop';
        audio.src = audioSource + '?' + Math.random();
        audio.play();
    } else {
        this.textContent = 'play';
        audio.src = '';
    }
});

const volume = document.createElement('input');
volume.type = 'range';
volume.min = 0;
volume.max = 100;
volume.value = 30;
volume.addEventListener('input', function(event){
    audio.volume = customVolume(this.value/100);
});

const mute = document.createElement('div');
mute.textContent = 'mute';
mute.style.cssText = 'display: inline-block; background: #ccc; height: 48px; line-height: 48px; width: 60px; cursor: pointer';
mute.addEventListener('click', function(event) {
    this.textContent = audio.muted ? 'mute' : 'unmute';
    audio.muted = !audio.muted;
});

const trackName = document.createElement('div');

function getTrack() {
    fetch(audioSource + '?' + Math.random(), { headers: {'Icy-MetaData' : 1}})
        .then(response => {
        if (response.status !== 200 ) {
            return Promise.reject(new Error(response.statusText));
        }
        return Promise.resolve(response);
    })
        .then(response => { return response.body.getReader()})
        .then(reader => {
        let charsReceived = 0;
        let responseString = '';
        let decoder = new TextDecoder('utf-8');
        reader.read().then(function processText({value}) {
            charsReceived += value.length;
            responseString += decoder.decode(value,{stream: true});
            if (charsReceived < 10000) {
                return reader.read().then(processText);
            }
            reader.cancel();
            return;
        }).then(function(){
            const titleMatch = responseString.match(/StreamTitle=\'(.*?)\';/);
            const djMatch = responseString.match(/icy-name:(.*)/);
            if (Array.isArray(titleMatch) && titleMatch[1]) {
                return titleMatch[1];
            }
            if (Array.isArray(djMatch) && djMatch[1]) {
                return djMatch[1];
            }
            return 'Radio Anonymous';
        }).then(currentTrackName => {trackName.textContent = currentTrackName});
    }).catch(error => {console.log('Failed to get track name.', error)});
}

getTrack();
setInterval(getTrack,10000);
newplayer.appendChild(audio);
newplayer.appendChild(trackName);
newplayer.appendChild(play);
newplayer.appendChild(mute);
newplayer.appendChild(volume);
player.replaceWith(newplayer);
