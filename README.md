# TWP html5 player contest

A simple html5 player with non default controls and track names for TWP contest.

It has play/pause button, mute button and a volume slider.  
Player can show now playing song.  
It has all features of now playing image and will display right song/dj name in some exceptional cases when image fails to display it right.

To obtain the current track this player uses function getTrack() which gets stream and extracts current song title and dj name.  
It uses modern browser js features such as promises.

The original script is intended to use with userscript extension like https://www.tampermonkey.net.  
Recommende browser versions:
  - Chrome: 42+
  - Firefox: 40+

However you should always use latest version of browsers.

Feel free to write issues and/or use it for your own players for contest.

Some screenshots:  
![Sample track 1](sample_track_1.png)
![Sample track 2](sample_track_2.png)
![No tracks show dj](no_tracks_show_dj.png)
